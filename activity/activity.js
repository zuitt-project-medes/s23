db.users.insertMany([
    {
        "firstName" : "sample1FN",
        "lastName" : "sample1LN",
        "email" : "sample1@mail.com",
        "password" : "sample1sample1",
        "isAdmin" : false
    },
    {
        "firstName" : "sample2FN",
        "lastName" : "sample2LN",
        "email" : "sample2@mail.com",
        "password" : "sample2sample2",
        "isAdmin" : false
    },
    {
        "firstName" : "sample3FN",
        "lastName" : "sample3LN",
        "email" : "sample3@mail.com",
        "password" : "sample3sample3",
        "isAdmin" : false
    },
    {
        "firstName" : "sample4FN",
        "lastName" : "sample4LN",
        "email" : "sample4@mail.com",
        "password" : "sample4sample4",
        "isAdmin" : false
    },
    {
        "firstName" : "sample5FN",
        "lastName" : "sample5LN",
        "email" : "sample5@mail.com",
        "password" : "sample5sample5",
        "isAdmin" : false
    }    
]);
    
db.courses.insertMany([
    {
        "name" : "Computer Programming 1",
        "price" : "2500",
        "isActive" : false
    },
    {
        "name" : "Computer Programming 2",
        "price" : "3500",
        "isActive" : false
    },
    {
        "name" : "Computer Programming 3",
        "price" : "4500",
        "isActive" : false
    }
]);
    
    
        db.users.find({})
        
        db.courses.find({})
        
        db.users.find({"isAdmin" : false})
        
        db.users.updateOne({"isAdmin" : false}, {$set: {"isAdmin" : true}})
        
        db.courses.updateOne({"name" : "Computer Programming 1"}, {$set: {"isActive" : true}})
        
        db.courses.deleteMany({"isActive" : false})
        
        